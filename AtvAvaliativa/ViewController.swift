//
//  ViewController.swift
//  AtvAvaliativa
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let email: String
    let endereco: String
    let numero: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereço.text = contato.endereco
        
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Enzo", email: "teste1@teste.com", endereco: "Endereço qualquer 1", numero: "31 97832-6409"))
        listaDeContatos.append(Contato(nome: "Eder", email: "teste2@teste.com", endereco: "Endereço qualquer 2", numero: "31 97463-2937"))
        listaDeContatos.append(Contato(nome: "Andressa", email: "teste3@teste.com", endereco: "Endereço qualquer 3", numero: "31 94861-1027"))
        listaDeContatos.append(Contato(nome: "Guilherme", email: "teste4@teste.com", endereco: "Endereço qualquer 4", numero: "31 95372-8412"))
    }


}

